package com.example.examen

class Rectangulo {
    public var base: Int
    public var altura: Int

    constructor(i: Int, i1: Int) {
        base = 0
        altura = 0
    }

    fun calcularArea(): Int {
        return base.toInt() * altura.toInt()
    }

    fun calcularPerimetro(): Int {
        return 2 * (base.toInt() + altura.toInt())
    }
}

