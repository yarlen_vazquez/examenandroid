package com.example.examen


import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity


class RectanguloActividad : AppCompatActivity() {
    private lateinit var lblBase: TextView
    private lateinit var lblAltura: TextView
    private lateinit var lblArea: TextView
    private lateinit var lblPerimetro: TextView
    private lateinit var lblNombre: TextView
    private lateinit var txtAltura: EditText
    private lateinit var txtBase: EditText
    private lateinit var btnCalcular: Button
    private lateinit var btnRegresar: Button
    private lateinit var btnLimpiar: Button

    private val rectangulo = Rectangulo(0, 0)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_rectangulo)
        inicializarComponentes()

        btnCalcular.setOnClickListener { calcular() }
        btnRegresar.setOnClickListener { btnRegresar() }
        btnLimpiar.setOnClickListener { limpiar() }
    }

    private fun inicializarComponentes() {
        lblBase = findViewById(R.id.lblBase)
        lblAltura = findViewById(R.id.lblAltura)
        lblArea = findViewById(R.id.lblArea)
        lblPerimetro = findViewById(R.id.lblPerimetro)
        lblNombre = findViewById(R.id.lblNombre)
        txtAltura = findViewById(R.id.txtAltura)
        txtBase = findViewById(R.id.txtBase)
        btnCalcular = findViewById(R.id.btnCalcular)
        btnLimpiar = findViewById(R.id.btnLimpiar)
        btnRegresar = findViewById(R.id.btnRegresar)

        val strNombre = intent.getStringExtra("strNombre")
        lblNombre.text = "Mi nombre es: $strNombre"
    }

    private fun btnRegresar() {
        AlertDialog.Builder(this)
            .setTitle("Rectangulo")
            .setMessage("¿Te gustaría regresar?")
            .setPositiveButton("Confirmar") { dialog, _ -> finish() }
            .setNegativeButton("Cancelar", null)
            .show()
    }

    private fun validarCampos(): Boolean {
        if (txtBase.text.toString().isEmpty() || txtAltura.text.toString().isEmpty()) {
            Toast.makeText(this@RectanguloActividad, "Campos Requeridos", Toast.LENGTH_SHORT).show()
            return false
        }
        return true
    }

    private fun actualizarCampos() {
        rectangulo.base = txtBase.text.toString().toInt()
        rectangulo.altura = txtAltura.text.toString().toInt()
    }

    private fun calcular() {
        if (validarCampos()) {
            actualizarCampos()
            val strArea = "Area: ${rectangulo.calcularArea()}"
            val strPerimetro = "Perimetro: ${rectangulo.calcularPerimetro()}"
            lblArea.text = strArea
            lblPerimetro.text = strPerimetro
        }
    }

    private fun limpiar() {
        txtAltura.setText("")
        txtBase.setText("")
        lblPerimetro.text = "Perimetro"
        lblArea.text = "Area"
    }
}